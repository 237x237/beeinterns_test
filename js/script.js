$(document).ready(function(){
    $('.card__list').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="slick-prev buttons__slider"> <img src="icon/button_left.svg"' +
            ' alt=""></button>',
        nextArrow: '<button type="button" class="slick-next buttons__slider"><img' +
            '  alt="" src="icon/button_right.svg"</button>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: false,
             
                }
            },
            {
                breakpoint: 768,
                settings: {
                    dots: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: false,
                }
            },
            {
                breakpoint: 500,
                settings: {
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                }
            }
            ]
        }
    );
});
