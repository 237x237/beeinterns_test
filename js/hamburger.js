window.addEventListener('DOMContentLoaded', () => {
    const menu = document.querySelector('.left-bar'),
        hamburger = document.querySelector('.hamburger');
    hamburger.addEventListener('click', () => {
        hamburger.classList.toggle('hamburger_active');
        menu.classList.toggle('left-bar_active');
    });
    
})