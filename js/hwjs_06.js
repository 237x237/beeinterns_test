let CARDS ={
    BASE: document.querySelector('.card-base'),
}
UI_ELEMENT = {
    BASIC_TITLES: CARDS.BASE.querySelectorAll('.card__title'),
    BASIC_TEXT: CARDS.BASE.querySelectorAll('.card__text'),
}

UI_ELEMENT.BASIC_TITLES.forEach((titleItem) =>
    titleItem.textContent= titleItem.textContent.toUpperCase())

UI_ELEMENT.BASIC_TEXT.forEach((textItem) =>
    (textItem.textContent.length < 20) ? true : textItem.textContent = textItem.textContent.slice(0,20) + '...'   )