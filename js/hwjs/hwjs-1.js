let myVar; // cоздаем переменную myVar с ключевым словом let
myVar = 5; // myVar присваиваем значение 5
let myNum; // Cоздаем переменную myNum с ключевым словом let
myNum = myVar; // myNum присваиваем значение переменной myVar, в итоге myNum = 5
myVar = 10; //Перезаписываем значение переменной myVar на 10
myNum = myVar; //Перезаписываем значение переменной myNum на myVar, в итоге myNum = 10