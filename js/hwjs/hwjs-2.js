const myVar;
myVar = 5;
const myNum;
myNum = myVar;
myVar = 10;
myNum = myVar;

/*
По правилам создания переменных с ключевым словом const
- их нельзя создавать без значения, правильнее было бы const
myVar = 5, а так же нельзя изменять значение примитивных типов,
поэтому в этом случае предпочтительнее let
*/
